#!/bin/bash
set echo off

PWD = $(pwd -P)

#Stop container
docker stop tp2tl

#Clean containers and volume (better version than prune)
docker rm tp2tl -v
docker volume rm tp2tl_vol

#Rebuild image (removing it before for better cleanup)
docker rmi tp2tl_image
set echo on

docker build --no-cache -t tp2tl_image -f ${PWD}/project/docker/Dockerfile .

#Creates volume
docker volume create --name tp2tl_vol --opt device=${PWD} --opt o=bind --opt type=none

#start container
docker run -p 5555:5556 --mount source=tp2tl_vol,target=/mnt/api/ -d --name tp2tl tp2tl_image
