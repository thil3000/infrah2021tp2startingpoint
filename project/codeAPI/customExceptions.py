class InitialUserException(Exception):
        pass

class NonExistingUserException(Exception):
        pass

class ExistingUserException(Exception):
        pass

class FileNotExistException(Exception):
        pass