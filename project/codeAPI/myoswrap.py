import subprocess
from codeAPI.customExceptions import *

def runCommandToAddUser(username):
	subprocess.run(['useradd', username])

def runCommandToRemoveUser(username):
	subprocess.run(['userdel', username])

def readFileByLine(filename):
	content = []
	try:
		f = open(filename, "r")
		for x in f:
			content.append(x)
		f.close()
	except:
		raise FileNotExistException('file does not exist')
	return content