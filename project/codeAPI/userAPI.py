from flask import *
from codeAPI import myoswrap
from codeAPI.customExceptions import *

app = Flask('TP2 API')

LOGFILE = '/var/log/auth.log'
USERFILE = '/etc/passwd'
OKCODE = 2000
INITIAL_USERS = ['root',
		'daemon',
		'bin',
		'sys',
		'sync',
		'games',
		'man',
		'lp',
		'mail',
		'news',
		'uucp',
		'proxy',
		'www-data',
		'backup',
		'list',
		'irc',
		'gnats',
		'nobody',
		'_apt',
		'Debian-exim']

def getUserList():
	users = []
	for line in myoswrap.readFileByLine(USERFILE):
		users.append(line.split(':')[0])
	return users

def isInitialUser(username):
	if username in INITIAL_USERS:
		return True
	return False

#Done classical method to back related route (unit test this)
def hello():
	return 'Welcome to user hot program!'

#Done route method (test with deployment tests)
@app.route('/')
def route_hello(): #pragma: no cover
	return jsonify({'code':OKCODE, 'msg':hello()})


#classical method to back related route
def getUsers():
	allUsers = getUserList()
	newUsers = []
	for user in allUsers:
		if not isInitialUser(user):
			newUsers.append(user)
	users = {'InitialUsers' : INITIAL_USERS, 'NewUsers' : newUsers}
	return users

#route method
@app.route('/getusers')
def route_getUsers(): #pragma: no cover
	return jsonify({'code': OKCODE, 'msg': getUsers()})


#done classical method to back related route
def delUser(username):
	if username in getUserList():
		if isInitialUser(username):
			raise InitialUserException('cannot delete an initial user.')
		else:
			myoswrap.runCommandToRemoveUser(username)
			return 'deleted ' + username
	raise NonExistingUserException('The specified user (' + username + ') does not exist, cannot perform delete operation.')

#done route method
@app.route('/deluser')
def route_delUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = delUser(username)
			return jsonify({'code':OKCODE, 'msg':rep})
		except InitialUserException as iue:
			return make_response(jsonify({'code':'1003', 'msg':str(iue)}), 400)
		except NonExistingUserException as neue:
			return make_response(jsonify({'code':'1002', 'msg':str(neue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for deletion.'}), 400)

#classical method to back related route
def resetUsers():
	allUsers = getUsers()
	newUsers = allUsers['NewUsers']
	msg = 0
	for user in newUsers:
		delUser(user)
		msg += 1
	return msg

#route method 
@app.route('/resetusers')
def route_resetUsers(): #pragma: no cover
	return jsonify({'code':OKCODE, 'msg': resetUsers()})

#classical method to back related route
def addUser(username):
	if username in getUserList():
		raise ExistingUserException('cannot add an existing user.')
	myoswrap.runCommandToAddUser(username)
	return username + ' added.'

#route method 
@app.route('/adduser')
def route_addUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = addUser(username)
			return jsonify({'code': OKCODE, 'msg':rep})
		except ExistingUserException as eue:
			return make_response(jsonify({'code':'1001', 'msg':str(eue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory.'}), 400)


#classical method to back related route
def getLogs():
	try:
		logfile = myoswrap.readFileByLine(LOGFILE)
	except FileNotExistException as fnee:
		raise FileNotExistException('Log ' + str(fnee))

	log = []
	for line in logfile:
		log.append(line)
	return log

#route method 
@app.route('/getlog')
def route_getLogs(): #pragma: no cover
	return jsonify({'code' : OKCODE, 'msg' : getLogs()})

if __name__ == "__main__": #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port=5556)