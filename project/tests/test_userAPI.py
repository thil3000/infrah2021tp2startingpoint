import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isInitialUserWithNonInitialUser(self, mock_oswrap):
		isRoyInitialUser = userAPI.isInitialUser('Roy')
		self.assertEqual(False,isRoyInitialUser)

		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isInitialUserWithInitialUser(self, mock_oswrap):
		isRootInitialUser = userAPI.isInitialUser('root')
		self.assertEqual(True,isRootInitialUser)

		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserList(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash','roy:x:0:0:roy:/home/roy:/bin/bash']
		listUsers = userAPI.getUserList()

		expected = ['root','roy']
		self.assertEqual(expected,listUsers)

		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogWithNoLog(self, mock_oswrap):
		with self.assertRaises(FileNotExistException) as context:
			mock_oswrap.readFileByLine.side_effect = FileNotExistException()
			log = userAPI.getLogs()

			mock_oswrap.readFileByLine.assert_called_with('/var/log/auth.log')
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogWithLog(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['logs']
		log = userAPI.getLogs()
		self.assertEqual(log[0],'logs')
			
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
		obj = userAPI.addUser('Roy')

		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_called_with('Roy')


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addExistingUser(self, mock_oswrap):
		with self.assertRaises(ExistingUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
			obj = userAPI.addUser('Roy')

			#check the three possible mock calls
			mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.runCommandToAddUser.assert_called_with('Roy')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserNoNewUsers(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = []
		users = userAPI.getUsers()
		self.assertEqual(users,{'InitialUsers' : userAPI.INITIAL_USERS, 'NewUsers' : []})

		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserOneNewUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		users = userAPI.getUsers()
		self.assertEqual(users,{'InitialUsers' : userAPI.INITIAL_USERS, 'NewUsers' : ['Roy']})

		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserMoreThenOneUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash','Garry:x:1000:1000:Garry:/home/Garry:/bin/bash']
		users = userAPI.getUsers()
		self.assertEqual(users,{'InitialUsers' : userAPI.INITIAL_USERS, 'NewUsers' : ['Roy','Garry']})

		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUserWithNoUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		userAPI.resetUsers()

		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUserWithOneUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:0:0:Roy:/home/roy:/bin/bash']
		nb = userAPI.resetUsers()

		self.assertEqual(1,nb)
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUserWithMultipleUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:0:0:Roy:/home/roy:/bin/bash','Garry:x:0:0:Garry:/home/garry:/bin/bash']
		nb = userAPI.resetUsers()

		self.assertEqual(2,nb)
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()

if __name__ == '__main__':
	unittest.main()
